Source: mailfilter
Section: mail
Priority: optional
Maintainer: Joerg Jaspert <joerg@debian.org>
Uploaders: Elimar Riesebieter <riesebie@lxtec.de>
Standards-Version: 4.6.1
Build-Depends: bison,
               debhelper-compat (= 13),
	       flex,
	       gettext,
	       libfl-dev,
	       libssl-dev
Homepage: https://mailfilter.sourceforge.io/
Vcs-Browser: https://salsa.debian.org/riesebie/mailfilter
Vcs-Git: https://salsa.debian.org/riesebie/mailfilter.git
Rules-Requires-Root: no

Package: mailfilter
Architecture: any
Depends: debconf | debconf-2.0, ${misc:Depends}, ${shlibs:Depends}
Description: Program that filters your incoming e-mail to help remove spam
 Mailfilter is very flexible utility for UNIX (-like) operating systems
 to get rid of unwanted e-mail messages, before having to go through the
 trouble of downloading them to the local computer. It offers support for
 one or many POP3 accounts and is especially useful for dialup connections
 via modem, ISDN, etc. Install Mailfilter if you'd like to remove spam from
 your POP3 mail accounts.
 .
 With Mailfilter you can define your own filters (rules) to determine
 which e-mails should be delivered and which are considered waste. Rules
 are Regular Expressions, so you can make use of familiar options from
 other mail delivery programs such as e.g. procmail.
 .
 If you do not get your mail from a POP3-Server you don't need Mailfilter.
