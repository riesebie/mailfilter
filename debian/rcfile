# -----------------------------------------------------------
# Logile path (be sure you have write permission in this
# directory; you MUST specify a logfile).  This one auto-
# matically stores the log files sorted by month and year.

LOGFILE = "$HOME/logs/mailfilter-`date +"%h%y"'"


# -----------------------------------------------------------
# Level of verbosity
#
# 0  Silent, show nothing at all
#
# 1  Only show errors
#
# 2  Only show "Deleted..." messages and errors
#
# 3  Default; Show "Deleted..." messages, errors and
#    "Examining..." messages
#
# 4  Like (3), except this also shows the current
#    account's username
#
# 5  Like (4), except this also shows which filter
#    matched which string of an e-mail header
#
# 6  Debugging mode; prints out almost everything

VERBOSE = 3


# -----------------------------------------------------------
# POP server list (do not change the order of the fields!)
# Note: Port 110 is usually the port APOP/POP3 servers use.
# When SSL comes into play, then it is usually 995.

SERVER   = "pop.server.com"
USER     = "username"
PASS     = "password"
PROTOCOL = "pop3"
PORT     = 110

SERVER   = "pop.secondserver.com"
USER     = "anotherusername"
PASS     = "anotherpassword"
PROTOCOL = "pop3/ssl"
PORT     = 995

SERVER   = "apop.server.com"
USER     = "yetanoterusername"
PASS     = "yetanotherpassword"
PROTOCOL = "apop/ssl"
PORT     = 995


# -----------------------------------------------------------
# Do you want case sensitive e-mail filters? { yes | no }

REG_CASE = "no"


# -----------------------------------------------------------
# Sets the type of Regular Expression used { extended | basic }
#
# (The default is 'basic', don't change unless you know what
# you are doing. Extended REs are more complex to set up.)

REG_TYPE = "basic"


# -----------------------------------------------------------
# Maximum e-mail size in bytes that should not be exceeded.

MAXSIZE_DENY = 1000000


# ----------------------------------------------------------
# Filter rules for detecting spam (each rule must be placed
# in a seperate line)

# These filters detect certain unpleasant e-mail subjects:
DENY = "^Subject:.*Get penis enlargement"
DENY = "^Subject:.*WIN MONEY"

# This one filters mail from a certain person:
DENY = "^From:.*spammer@any_spam_organisation.com"

# This one filters mail from everyone at a certain
# organisation:
DENY = "^From:.*@any_provider_that_spams.org"

# We don't want any of those 'LEGAL' messages either
# while stuff with 'legal' in the subject still interests us:
DENY_CASE = "^Subject:.*LEGAL"


# -----------------------------------------------------------
# Normalises the subject strings before parsing, e.g.
# ',L.E-G,A.L; ,C.A-B`L`E, +.B-O`X` ;D`E`S,C;R,A.MB;L,E.R-]'
# becomes 'LEGAL CABLE BOX DESCRAMBLER' which can be filtered.

NORMAL = "yes"


# -----------------------------------------------------------
# The maximum e-mail size in bytes that messages from friends
# should not exceed. Set this to 0 if all your friends (ALLOW)
# can send messages as long as they want.

MAXSIZE_ALLOW = 0


# ----------------------------------------------------------
# Set list of friends that always pass, if they do not
# exceed the message length of MAXSIZE_ALLOW

# This rule allows all mail from a friend who was unlucky
# enough to have signed up with a spam organisation. With DENY
# we block everyone else from that domain though! See above!
ALLOW = "^From:.*a_friend_with_account@any_provider_that_spams.org"

# Of course we allow e-mail from anyone who has something to
# say about mailfilter:
ALLOW = "^Subject:.*mailfilter"

# We also let our girlfriend send any e-mail she wants:
ALLOW = "^From:.*my_girlfriend@any_provider.com"
