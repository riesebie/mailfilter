Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: mailfilter
Upstream-Contact: Andreas Bauer baueran@users.sourceforge.net
Source: http://mailfilter.sourceforge.net
Comment: This package was debianized by Joerg Jaspert <joerg@debian.org> on
         Fri, 22 Jun 2001 16:51:47 +0200.

Files: *
Copyright: 2000 - 2022 Andreas Bauer <baueran@in.tum.de>
License: GPL-2+ with OpenSSL exception

License: GPL-2+ with OpenSSL exception
 This program is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later
 version.
 .
 In addition, as a special exception, the author of this
 program gives permission to link the code of its
 release with the OpenSSL project's "OpenSSL" library (or
 with modified versions of it that use the same license as
 the "OpenSSL" library), and distribute the linked
 executables. You must obey the GNU General Public
 License in all respects for all of the code used other
 than "OpenSSL".  If you modify this file, you may extend
 this exception to your version of the file, but you are
 not obligated to do so.  If you do not wish to do so,
 delete this exception statement from your version.
 .
 This program is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public
 License along with this package; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

Files: src/getopt.c  src/getopt1.c  src/getopt.h
Copyright: 1987 88 89 90 91 92 93 94 95 96 98 99 2000 2001 Free Software Foundation, Inc.
Comment: This file is part of the GNU C Library.
License: LPGL-2.1
 This package is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this package; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License can be found in `/usr/share/common-licenses/LGPL-2.1'.

Files: src/md5c.c src/md5.h
Copyright: 1991-2, RSA Data Security, Inc. Created 1991.
License: permissive
 All rights reserved.
 License to copy and use this software is granted provided that it
 is identified as the "RSA Data Security, Inc. MD5 Message-Digest
 Algorithm" in all material mentioning or referencing this software
 or this function.
 .
 License is also granted to make and use derivative works provided
 that such works are identified as "derived from the RSA Data
 Security, Inc. MD5 Message-Digest Algorithm" in all material
 mentioning or referencing the derived work.
 RSA Data Security, Inc. makes no representations concerning either
 the merchantability of this software or the suitability of this
 software for any particular purpose. It is provided "as is"
 without express or implied warranty of any kind.
 .
 These notices must be retained in any copies of any part of this
 documentation and/or software.

Files: contrib/checkfilter.sh contrib/deleted.sh contrib/examined.sh
       contrib/getmailer.pl contrib/getstats.pl contrib/prozente.pl
       contrib/runit.sh contrib/sort_de_do.sh contrib/xmailer.sh
Copyright: 2003 Kay Schulz <kay@kay-schulz.com>
License: GPL-2+

Files: contrib/chrcformat_05-07 contrib/rmcrlf
Copyright: 2003 Til Schubbe <t.schubbe@gmx.de>
License: GPL-2+

Files: contrib/mfdelete.stat
Copyright: Tim Moore <timothymoore@nsr500.net>
License: GPL-2+

Files: contrib/spam.tar.gz
Copyright: 2 Franck Pommereau <pommereau@univ-paris12.fr>
License: GPL-2

License: GPL-2
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

Files: debian/*
Copyright: 2001-2016 Joerg Jaspert <joerg@debian.org>
           2009-2016 Ricardo Mones <mones@debian.org>
           2016-2022 Elimar Riesebieter <riesebie@lxtec.de>
License: GPL-2+

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'
